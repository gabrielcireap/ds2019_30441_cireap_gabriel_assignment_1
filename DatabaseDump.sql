CREATE DATABASE  IF NOT EXISTS `immplah` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `immplah`;
-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: immplah
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `medication`
--

DROP TABLE IF EXISTS `medication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `medication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `side_effects` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication`
--

LOCK TABLES `medication` WRITE;
/*!40000 ALTER TABLE `medication` DISABLE KEYS */;
INSERT INTO `medication` VALUES (1,'Ibuprofen','Allergy'),(2,'Olynth','Addiction'),(3,'Algocalmin','Headache');
/*!40000 ALTER TABLE `medication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication_per_plan`
--

DROP TABLE IF EXISTS `medication_per_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `medication_per_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dosage` double DEFAULT NULL,
  `intake_moments` varchar(255) DEFAULT NULL,
  `medication_id` int(11) DEFAULT NULL,
  `medication_plan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgn2e42ex4j0duqnlu7njv8paj` (`medication_id`),
  KEY `FKfkce1m2eh0v32gp5hp5dxg09c` (`medication_plan_id`),
  CONSTRAINT `FKfkce1m2eh0v32gp5hp5dxg09c` FOREIGN KEY (`medication_plan_id`) REFERENCES `medication_plan` (`id`),
  CONSTRAINT `FKgn2e42ex4j0duqnlu7njv8paj` FOREIGN KEY (`medication_id`) REFERENCES `medication` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication_per_plan`
--

LOCK TABLES `medication_per_plan` WRITE;
/*!40000 ALTER TABLE `medication_per_plan` DISABLE KEYS */;
INSERT INTO `medication_per_plan` VALUES (1,3,'seara',1,1),(2,5,'dimineata',2,1),(3,1,'dimineata',3,1);
/*!40000 ALTER TABLE `medication_per_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication_plan`
--

DROP TABLE IF EXISTS `medication_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `medication_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `end_time` date DEFAULT NULL,
  `start_time` date DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKsbssmqwap7gwimwcnwmolheef` (`patient_id`),
  CONSTRAINT `FKsbssmqwap7gwimwcnwmolheef` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication_plan`
--

LOCK TABLES `medication_plan` WRITE;
/*!40000 ALTER TABLE `medication_plan` DISABLE KEYS */;
INSERT INTO `medication_plan` VALUES (1,'2019-10-21','2019-10-19',2);
/*!40000 ALTER TABLE `medication_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `medical_record` varchar(255) DEFAULT NULL,
  `caregiver_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6xsugcipalsc4u3i7db2oob99` (`caregiver_id`),
  KEY `FKs803llq8v2bbsy5sqmahjxc4j` (`doctor_id`),
  KEY `FKp6ttmfrxo2ejiunew4ov805uc` (`user_id`),
  CONSTRAINT `FK6xsugcipalsc4u3i7db2oob99` FOREIGN KEY (`caregiver_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKp6ttmfrxo2ejiunew4ov805uc` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKs803llq8v2bbsy5sqmahjxc4j` FOREIGN KEY (`doctor_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (2,'Nimic',5,1,7);
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'str. Popesti nr. 7, Popesti-Leordeni','1990-03-31','popescu.ion@gmail.com','MALE','Popescu Ion Andrei','$2a$10$kgMU.lpLodmDQn2ArjB6teZU4EltQWouQwyH4kFYRCtc0rzRWzpPq','DOCTOR','popescu.ion'),(2,'str. Ionesti, Bucuresti','1990-12-14','ionescu.marian@yahoo.com','MALE','Ionescu Marian','$2a$10$QMd8xxQpVOC0XxbhgIdlGePPjua/glxiEqDS1F9RHcCkfxkvqK.HO','DOCTOR','ionescu.marian'),(4,'str. Soporului nr.3','1990-12-14','morosan.dan@gmail.com','MALE','Morosan Dan','$2a$10$.sjIzFfBuIz9tsXkxEi72eRIQpCwwxNwGrkvsnekuvsvAvhSSR4XS','CAREGIVER','morosan.dan'),(5,'str. Soporului nr.4','1991-09-08','morosan.diana@gmail.com','FEMALE','Morosan Diana','$2a$10$RgBW1C3BbF6x5yEQS78sweX1kJ1RJHHuorMSc18Xf7jn5C/Fzy10K','CAREGIVER','morosan.diana'),(7,'str. Calea Dorobantilor nr. 102, ap. 19','1990-12-14','gabriel.cireap@yahoo.com','FEMALE','Dragos Gabriel Cireap','$2a$10$N8NeOli8tfeLD3jUZxBC0ON5AJBvxOUv8CXZwKNrNi0r7WbbdVrbS','PATIENT','cireap.gabriel');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'immplah'
--

--
-- Dumping routines for database 'immplah'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-23  1:20:04

package com.gabrielcireap.assignment1.config;

import com.gabrielcireap.assignment1.entity.Role;
import com.gabrielcireap.assignment1.service.LoginUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final LoginUserDetailsService loginUserDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/patients/{id}/plan/user").hasAuthority(Role.PATIENT.name())
                .antMatchers(HttpMethod.GET, "/patients/{id}/plan").hasAnyAuthority(Role.CAREGIVER.name(), Role.DOCTOR.name(), Role.PATIENT.name())
                .antMatchers(HttpMethod.GET, "/patients/{id}").hasAnyAuthority(Role.PATIENT.name(), Role.DOCTOR.name())
                .antMatchers(HttpMethod.PUT, "/patients").hasAnyAuthority(Role.PATIENT.name(), Role.DOCTOR.name())
                .antMatchers("/patients/**").hasAuthority(Role.DOCTOR.name())
                .antMatchers("/caregivers/{id}/**").hasAnyAuthority(Role.CAREGIVER.name(), Role.DOCTOR.name())
                .antMatchers("/caregivers/**").hasAuthority(Role.DOCTOR.name())
                .antMatchers("/medications").hasAuthority(Role.DOCTOR.name())
                .antMatchers("/doctors").hasAuthority(Role.DOCTOR.name())
                .antMatchers(HttpMethod.POST, "/users").permitAll()
                .anyRequest().authenticated().and()
                .httpBasic().and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .cors().and()
                .csrf().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(loginUserDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

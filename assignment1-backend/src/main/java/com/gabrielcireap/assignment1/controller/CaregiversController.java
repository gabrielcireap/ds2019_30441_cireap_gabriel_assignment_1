package com.gabrielcireap.assignment1.controller;

import com.gabrielcireap.assignment1.dto.PatientInfoDto;
import com.gabrielcireap.assignment1.dto.UserInfoDto;
import com.gabrielcireap.assignment1.service.CaregiverManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CaregiversController {
    private final CaregiverManagementService caregiverManagementService;

    @GetMapping("/caregiver/{id}/patients")
    public List<PatientInfoDto> getCaregiversPatients(@PathVariable int id) {
        return caregiverManagementService.getCaregiversPatients(id);
    }

    @GetMapping("/caregivers")
    public List<UserInfoDto> findAllCaregivers() {
        return caregiverManagementService.findAllCaregivers();
    }
}

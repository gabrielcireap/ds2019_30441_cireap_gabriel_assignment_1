package com.gabrielcireap.assignment1.controller;

import com.gabrielcireap.assignment1.dto.UserInfoDto;
import com.gabrielcireap.assignment1.service.DoctorManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class DoctorsController {
    private final DoctorManagementService doctorManagementService;

    @GetMapping("/doctors")
    public List<UserInfoDto> findAllDoctors() {
        return doctorManagementService.findAllDoctors();
    }
}

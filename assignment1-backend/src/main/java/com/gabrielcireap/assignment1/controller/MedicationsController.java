package com.gabrielcireap.assignment1.controller;

import com.gabrielcireap.assignment1.dto.MedicationDto;
import com.gabrielcireap.assignment1.dto.MedicationSaveDto;
import com.gabrielcireap.assignment1.service.MedicationManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MedicationsController {
    private final MedicationManagementService medicationManagementService;

    @PostMapping("/medications")
    public MedicationDto save(@RequestBody MedicationSaveDto medication) {
        return medicationManagementService.save(medication);
    }

    @PutMapping("/medications")
    public MedicationDto update(@RequestBody MedicationDto medicationDto) {
        return medicationManagementService.update(medicationDto);
    }

    @GetMapping("/medications")
    public List<MedicationDto> findAll() {
        return medicationManagementService.findAll();
    }

    @GetMapping("/medications/{id}")
    public MedicationDto findById(@PathVariable int id) {
        return medicationManagementService.findById(id);
    }

    @DeleteMapping("/medications/{id}")
    public void delete(@PathVariable int id) {
        medicationManagementService.delete(id);
    }
}

package com.gabrielcireap.assignment1.controller;

import com.gabrielcireap.assignment1.dto.*;
import com.gabrielcireap.assignment1.service.PatientManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class PatientsController {
    private final PatientManagementService patientManagementService;

    @GetMapping("/patients")
    public List<PatientInfoDto> findAll() {
        return patientManagementService.findAll();
    }

    @GetMapping("/patients/{id}")
    public PatientInfoDto findById(@PathVariable int id) {
        return patientManagementService.findByUserId(id);
    }

    @PutMapping("/patients")
    public PatientInfoDto update(@RequestBody PatientUpdateDto patientUpdateDto) {
        return patientManagementService.update(patientUpdateDto);
    }

    @PostMapping("/patients")
    public PatientInfoDto save(@RequestBody PatientSaveDto patientSaveDto) {
        return patientManagementService.save(patientSaveDto);
    }

    @DeleteMapping("/patients/{id}")
    public void delete(@PathVariable int id) {
        patientManagementService.delete(id);
    }

    @GetMapping("/patients/{id}/plan")
    public List<MedicationPlanDto> getMedicationPlans(@PathVariable int id) {
        return patientManagementService.getMedicationPlans(id);
    }

    @GetMapping("/patients/{id}/plan/user")
    public List<MedicationPlanDto> getMedicationPlansByUserId(@PathVariable int id) {
        return patientManagementService.getMedicationPlansByUserId(id);
    }

    @PostMapping("/patients/{id}/plan")
    public int saveMedicationPlan(@PathVariable int id, @RequestBody MedicationPlanDto medicationPlanDto) {
        return patientManagementService.saveMedicationPlan(id, medicationPlanDto);
    }
}

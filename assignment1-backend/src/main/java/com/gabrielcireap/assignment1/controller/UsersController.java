package com.gabrielcireap.assignment1.controller;

import com.gabrielcireap.assignment1.dto.UserInfoDto;
import com.gabrielcireap.assignment1.dto.UserLoginDto;
import com.gabrielcireap.assignment1.dto.UserSaveDto;
import com.gabrielcireap.assignment1.dto.UserUpdateDto;
import com.gabrielcireap.assignment1.service.UserManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UsersController {
    private final UserManagementService userManagementService;

    @PostMapping("/users")
    public UserInfoDto save(@RequestBody UserSaveDto user) {
        return userManagementService.save(user);
    }

    @PutMapping("/users")
    public UserInfoDto update(@RequestBody UserUpdateDto user) {
        return userManagementService.update(user);
    }

    @GetMapping("/users")
    public List<UserInfoDto> findAll() {
        return userManagementService.findAll();
    }

    @GetMapping("/users/{id}")
    public UserInfoDto findById(@PathVariable int id) {
        return userManagementService.findById(id);
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable int id) {
        userManagementService.delete(id);
    }

    @PostMapping("/login")
    public UserInfoDto login(@RequestBody UserLoginDto userLoginDto) {
        return userManagementService.login(userLoginDto);
    }
}

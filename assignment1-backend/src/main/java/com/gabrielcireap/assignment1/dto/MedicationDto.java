package com.gabrielcireap.assignment1.dto;

import lombok.Data;

@Data
public class MedicationDto {
    private Integer id;
    private String name;
    private String sideEffects;
}

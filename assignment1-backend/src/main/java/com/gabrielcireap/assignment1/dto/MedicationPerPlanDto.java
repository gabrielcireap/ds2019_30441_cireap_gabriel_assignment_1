package com.gabrielcireap.assignment1.dto;

import lombok.Data;

@Data
public class MedicationPerPlanDto {
    private Integer id;
    private MedicationDto medicationDto;
    private Double dosage;
    private String intakeMoments;
}

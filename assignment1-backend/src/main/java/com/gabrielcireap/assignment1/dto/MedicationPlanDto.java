package com.gabrielcireap.assignment1.dto;

import lombok.Data;

import java.sql.Date;
import java.util.List;

@Data
public class MedicationPlanDto {
    private Integer id;
    private Date startTime;
    private Date endTime;
    private List<MedicationPerPlanDto> medicationPerPlanDtoList;
}

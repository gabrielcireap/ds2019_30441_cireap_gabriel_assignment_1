package com.gabrielcireap.assignment1.dto;

import lombok.Data;

@Data
public class MedicationSaveDto {
    private String name;
    private String sideEffects;
}

package com.gabrielcireap.assignment1.dto;

import lombok.Data;

@Data
public class PatientInfoDto {
    private Integer id;
    private UserInfoDto user;
    private UserInfoDto doctor;
    private UserInfoDto caregiver;
    private String medicalRecord;
}

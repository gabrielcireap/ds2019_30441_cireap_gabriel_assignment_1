package com.gabrielcireap.assignment1.dto;

import lombok.Data;

@Data
public class PatientSaveDto {
    private UserSaveDto user;
    private Integer doctorId;
    private Integer caregiverId;
    private String medicalRecord;
}

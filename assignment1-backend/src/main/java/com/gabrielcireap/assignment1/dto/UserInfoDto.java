package com.gabrielcireap.assignment1.dto;

import com.gabrielcireap.assignment1.entity.Gender;
import lombok.Data;
import java.sql.Date;

@Data
public class UserInfoDto {
    private Integer id;
    private String name;
    private Gender gender;
    private Date birthDate;
    private String address;
    private String role;
    private String email;
}

package com.gabrielcireap.assignment1.dto;

import lombok.Data;

@Data
public class UserLoginDto {
    private String username;
    private String password;
}

package com.gabrielcireap.assignment1.dto;
import lombok.Data;

@Data
public class UserSaveDto {
    private Integer id;
    private String name;
    private String gender;
    private String birthDate;
    private String address;
    private String username;
    private String password;
    private String role;
    private String email;
}

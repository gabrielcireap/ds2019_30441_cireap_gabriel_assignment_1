package com.gabrielcireap.assignment1.dto;

import lombok.Data;

@Data
public class UserUpdateDto {
    private Integer id;
    private String name;
    private String address;
    private String password;
    private String email;
}

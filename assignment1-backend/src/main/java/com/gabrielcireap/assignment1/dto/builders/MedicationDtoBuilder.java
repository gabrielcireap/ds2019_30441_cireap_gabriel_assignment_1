package com.gabrielcireap.assignment1.dto.builders;

import com.gabrielcireap.assignment1.dto.MedicationDto;
import com.gabrielcireap.assignment1.entity.Medication;

public class MedicationDtoBuilder {
    public static MedicationDto fromEntity(Medication medication) {
        MedicationDto medicationDto = new MedicationDto();
        medicationDto.setId(medication.getId());
        medicationDto.setName(medication.getName());
        medicationDto.setSideEffects(medication.getSideEffects());
        return medicationDto;
    }

    public static Medication toEntity(MedicationDto medicationDto) {
        return new Medication(medicationDto.getId(), medicationDto.getName(), medicationDto.getSideEffects());
    }
}

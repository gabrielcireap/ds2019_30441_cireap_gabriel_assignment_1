package com.gabrielcireap.assignment1.dto.builders;

import com.gabrielcireap.assignment1.dto.MedicationPerPlanDto;
import com.gabrielcireap.assignment1.entity.MedicationPerPlan;
import com.gabrielcireap.assignment1.entity.MedicationPlan;

public class MedicationPerPlanDtoBuilder {
    public static MedicationPerPlanDto fromEntity(MedicationPerPlan medicationPerPlan) {
        MedicationPerPlanDto medicationPerPlanDto = new MedicationPerPlanDto();
        medicationPerPlanDto.setDosage(medicationPerPlan.getDosage());
        medicationPerPlanDto.setId(medicationPerPlan.getId());
        medicationPerPlanDto.setIntakeMoments(medicationPerPlan.getIntakeMoments());
        medicationPerPlanDto.setMedicationDto(MedicationDtoBuilder.fromEntity(medicationPerPlan.getMedication()));
        return medicationPerPlanDto;
    }

    public static MedicationPerPlan toEntity(MedicationPerPlanDto medicationPerPlanDto, MedicationPlan medicationPlan) {
        MedicationPerPlan medicationPerPlan = new MedicationPerPlan();
        medicationPerPlan.setId(medicationPerPlanDto.getId());
        medicationPerPlan.setDosage(medicationPerPlanDto.getDosage());
        medicationPerPlan.setIntakeMoments(medicationPerPlanDto.getIntakeMoments());
        medicationPerPlan.setMedicationPlan(medicationPlan);
        medicationPerPlan.setMedication(MedicationDtoBuilder.toEntity(medicationPerPlanDto.getMedicationDto()));
        return medicationPerPlan;
    }
}

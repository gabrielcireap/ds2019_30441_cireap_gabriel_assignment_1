package com.gabrielcireap.assignment1.dto.builders;

import com.gabrielcireap.assignment1.dto.MedicationPerPlanDto;
import com.gabrielcireap.assignment1.dto.MedicationPlanDto;
import com.gabrielcireap.assignment1.entity.MedicationPlan;
import com.gabrielcireap.assignment1.entity.Patient;

import java.util.List;

public class MedicationPlanDtoBuilder {
    public static MedicationPlanDto fromEntity(MedicationPlan medicationPlan, List<MedicationPerPlanDto> medicationPerPlanDtoList) {
        MedicationPlanDto medicationPlanDto = new MedicationPlanDto();
        medicationPlanDto.setEndTime(medicationPlan.getEndTime());
        medicationPlanDto.setStartTime(medicationPlan.getStartTime());
        medicationPlanDto.setId(medicationPlan.getId());
        medicationPlanDto.setMedicationPerPlanDtoList(medicationPerPlanDtoList);
        return medicationPlanDto;
    }

    public static MedicationPlan toEntity(MedicationPlanDto medicationPlanDto, Patient patient) {
        MedicationPlan medicationPlan = new MedicationPlan();
        medicationPlan.setId(medicationPlanDto.getId());
        medicationPlan.setEndTime(medicationPlanDto.getEndTime());
        medicationPlan.setStartTime(medicationPlanDto.getStartTime());
        medicationPlan.setPatient(patient);
        return medicationPlan;
    }
}

package com.gabrielcireap.assignment1.dto.builders;

import com.gabrielcireap.assignment1.dto.MedicationSaveDto;
import com.gabrielcireap.assignment1.entity.Medication;

public class MedicationSaveDtoBuilder {
    public static Medication toEntity(MedicationSaveDto medicationSaveDto) {
        return new Medication(null, medicationSaveDto.getName(), medicationSaveDto.getSideEffects());
    }
}

package com.gabrielcireap.assignment1.dto.builders;

import com.gabrielcireap.assignment1.dto.PatientInfoDto;
import com.gabrielcireap.assignment1.entity.Patient;

public class PatientInfoDtoBuilder {
    public static PatientInfoDto fromEntity(Patient patient) {
        PatientInfoDto patientInfoDto = new PatientInfoDto();
        patientInfoDto.setId(patient.getId());
        patientInfoDto.setMedicalRecord(patient.getMedicalRecord());
        patientInfoDto.setUser(UserInfoDtoBuilder.fromEntity(patient.getUser()));
        patientInfoDto.setDoctor(UserInfoDtoBuilder.fromEntity(patient.getDoctor()));
        patientInfoDto.setCaregiver(UserInfoDtoBuilder.fromEntity(patient.getCaregiver()));
        return patientInfoDto;
    }
}

package com.gabrielcireap.assignment1.dto.builders;

import com.gabrielcireap.assignment1.dto.PatientSaveDto;
import com.gabrielcireap.assignment1.entity.Patient;

public class PatientSaveDtoBuilder {
    public static Patient toEntity(PatientSaveDto patientSaveDto) {
        Patient patient = new Patient();
        patient.setMedicalRecord(patientSaveDto.getMedicalRecord());
        patient.setUser(UserSaveDtoBuilder.toEntity(patientSaveDto.getUser()));
        return patient;
    }
}

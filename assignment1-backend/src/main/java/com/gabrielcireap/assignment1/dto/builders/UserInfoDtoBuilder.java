package com.gabrielcireap.assignment1.dto.builders;

import com.gabrielcireap.assignment1.dto.UserInfoDto;
import com.gabrielcireap.assignment1.entity.Role;
import com.gabrielcireap.assignment1.entity.User;

public class UserInfoDtoBuilder {
    public static UserInfoDto fromEntity(User user) {
        UserInfoDto userInfoDto = new UserInfoDto();
        userInfoDto.setId(user.getId());
        userInfoDto.setName(user.getName());
        userInfoDto.setGender(user.getGender());
        userInfoDto.setBirthDate(user.getBirthDate());
        userInfoDto.setAddress(user.getAddress());
        userInfoDto.setRole(user.getRole().name());
        userInfoDto.setEmail(user.getEmail());
        return userInfoDto;
    }

    public static User toEntity(UserInfoDto userInfoDto) {
        User user = new User();
        user.setId(userInfoDto.getId());
        user.setName(userInfoDto.getName());
        user.setAddress(userInfoDto.getAddress());
        user.setBirthDate(userInfoDto.getBirthDate());
        user.setGender(userInfoDto.getGender());
        user.setEmail(userInfoDto.getEmail());

        switch(userInfoDto.getRole()) {
            case "DOCTOR":
                user.setRole(Role.DOCTOR);
                break;
            case "PATIENT":
                user.setRole(Role.PATIENT);
                break;
            default:
                user.setRole(Role.CAREGIVER);
                break;
        }

        return user;
    }
}

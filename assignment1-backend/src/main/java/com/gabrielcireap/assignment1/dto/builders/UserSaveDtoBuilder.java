package com.gabrielcireap.assignment1.dto.builders;

import com.gabrielcireap.assignment1.dto.UserSaveDto;
import com.gabrielcireap.assignment1.entity.Gender;
import com.gabrielcireap.assignment1.entity.Role;
import com.gabrielcireap.assignment1.entity.User;

import java.sql.Date;

public class UserSaveDtoBuilder {
    public static User toEntity(UserSaveDto userSaveDto) {
        User user = new User();
        user.setId(userSaveDto.getId());
        user.setUsername(userSaveDto.getUsername());
        user.setPassword(userSaveDto.getPassword());
        user.setName(userSaveDto.getName());
        user.setAddress(userSaveDto.getAddress());
        user.setEmail(userSaveDto.getEmail());

        switch(userSaveDto.getGender()) {
            case "MALE":
                user.setGender(Gender.MALE);
                break;
            default:
                user.setGender(Gender.FEMALE);
                break;
        }

        switch(userSaveDto.getRole()) {
            case "DOCTOR":
                user.setRole(Role.DOCTOR);
                break;
            case "PATIENT":
                user.setRole(Role.PATIENT);
                break;
            default:
                user.setRole(Role.CAREGIVER);
                break;
        }

        user.setBirthDate(Date.valueOf(userSaveDto.getBirthDate()));
        return user;
    }
}

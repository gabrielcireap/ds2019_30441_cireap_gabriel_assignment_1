package com.gabrielcireap.assignment1.entity;

public enum Gender {
    MALE,
    FEMALE
}

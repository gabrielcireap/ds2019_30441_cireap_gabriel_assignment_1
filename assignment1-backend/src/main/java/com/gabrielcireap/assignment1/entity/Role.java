package com.gabrielcireap.assignment1.entity;

public enum Role {
    DOCTOR,
    PATIENT,
    CAREGIVER
}

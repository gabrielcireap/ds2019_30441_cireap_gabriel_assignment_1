package com.gabrielcireap.assignment1.errorhandler;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class DuplicateUserException extends RuntimeException {
    private static final String MESSAGE = "The user already exists!";

    public DuplicateUserException() {
        super(MESSAGE);
    }
}

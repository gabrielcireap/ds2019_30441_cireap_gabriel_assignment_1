package com.gabrielcireap.assignment1.errorhandler;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class EntityValidationException extends RuntimeException {
    private static final String MESSAGE = "Entity could not be processed !";

    private final String resource;
    private final List<String> validationErrors;

    public EntityValidationException(String resource, List<String> errors) {
        super(MESSAGE);
        this.resource = resource;
        this.validationErrors = errors;
    }
}
package com.gabrielcireap.assignment1.errorhandler;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.List;

@Data
public class ExceptionHandlerResponseDto {
    private Date timestamp;
    private String entity;
    private String requestedUri;
    private List<String> details;
    private int statusCode;
    private String statusMessage;

    public ExceptionHandlerResponseDto(String entity, HttpStatus status, List<String> details, String requestedUri) {
        this.timestamp = new Date();
        this.entity = entity;
        this.statusCode = status.value();
        this.statusMessage = status.getReasonPhrase();
        this.details = details;
        this.requestedUri = requestedUri;
    }
}
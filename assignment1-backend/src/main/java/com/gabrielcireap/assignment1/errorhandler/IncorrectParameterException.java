package com.gabrielcireap.assignment1.errorhandler;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class IncorrectParameterException extends RuntimeException {
    private static final String MESSAGE = "Incorrect Parameters";
    private final String resource;
    private final List<String> invalidParams;

    public IncorrectParameterException(String resource, List<String> errors) {
        super(MESSAGE);
        this.resource = resource;
        this.invalidParams = errors;
    }
}
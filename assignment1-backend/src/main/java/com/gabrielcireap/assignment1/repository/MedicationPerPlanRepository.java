package com.gabrielcireap.assignment1.repository;

import com.gabrielcireap.assignment1.entity.MedicationPerPlan;
import com.gabrielcireap.assignment1.entity.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MedicationPerPlanRepository extends JpaRepository<MedicationPerPlan, Integer> {

    @Query(value = "SELECT medicationPerPlan " +
            "FROM MedicationPerPlan medicationPerPlan " +
            "WHERE medicationPerPlan.medicationPlan.id = :medicationPlanId")
    List<MedicationPerPlan> getMedicationPerPlanByMedicationPlanId(int medicationPlanId);
}

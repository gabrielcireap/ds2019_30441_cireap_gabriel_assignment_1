package com.gabrielcireap.assignment1.repository;

import com.gabrielcireap.assignment1.entity.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {

    @Query(value = "SELECT medicationPlan " +
            "FROM MedicationPlan medicationPlan " +
            "WHERE medicationPlan.patient.id = :patientId")
    List<MedicationPlan> getMedicationsPlanByPatientId(int patientId);

    @Query(value = "SELECT medicationPlan " +
            "FROM MedicationPlan medicationPlan " +
            "WHERE medicationPlan.patient.user.id = :userId")
    List<MedicationPlan> getMedicationsPlanByPatientUsersId(int userId);
}

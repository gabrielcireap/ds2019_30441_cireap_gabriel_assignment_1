package com.gabrielcireap.assignment1.repository;

import com.gabrielcireap.assignment1.entity.Patient;
import com.gabrielcireap.assignment1.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {

    @Query(value = "SELECT patient " +
            "FROM Patient patient " +
            "WHERE patient.caregiver.id = :caregiverId")
    List<Patient> findPatientByCaregiverId(int caregiverId);

    @Query(value = "SELECT patient " +
            "FROM Patient patient " +
            "WHERE patient.user.id = :userId")
    Optional<Patient> findByUserId(int userId);
}

package com.gabrielcireap.assignment1.repository;

import com.gabrielcireap.assignment1.entity.Role;
import com.gabrielcireap.assignment1.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query(value = "SELECT user " +
            "FROM User user " +
            "WHERE user.username = :username")
    Optional<User> findUserByUsername(String username);

    Optional<User> findUserByEmail(String email);

    @Query(value = "SELECT user " +
            "FROM User user " +
            "WHERE user.role = :role")
    List<User> findAllByRole(Role role);
}

package com.gabrielcireap.assignment1.seed;

import com.gabrielcireap.assignment1.dto.UserSaveDto;
import com.gabrielcireap.assignment1.entity.Gender;
import com.gabrielcireap.assignment1.entity.Role;
import com.gabrielcireap.assignment1.entity.User;
import com.gabrielcireap.assignment1.repository.UserRepository;
import com.gabrielcireap.assignment1.service.UserManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.persistence.GeneratedValue;

@Component
@RequiredArgsConstructor
public class DoctorSeed implements CommandLineRunner {
    private final UserManagementService userManagementService;

    @Override
    public void run(String... args) throws Exception {
        if(userManagementService.findByRole(Role.DOCTOR).isEmpty()) {
            UserSaveDto userSaveDto = new UserSaveDto();
            userSaveDto.setAddress("str. Popesti nr. 7, Bucuresti");
            userSaveDto.setBirthDate("1990-04-01");
            userSaveDto.setEmail("popescu.ion@gmail.com");
            userSaveDto.setGender(Gender.MALE.name());
            userSaveDto.setName("Popescu Ion");
            userSaveDto.setPassword("123456");
            userSaveDto.setRole(Role.DOCTOR.name());
            userSaveDto.setUsername("popescu.ion");
            userManagementService.save(userSaveDto);
        }
    }
}

package com.gabrielcireap.assignment1.service;

import com.gabrielcireap.assignment1.dto.PatientInfoDto;
import com.gabrielcireap.assignment1.dto.UserInfoDto;
import com.gabrielcireap.assignment1.dto.builders.PatientInfoDtoBuilder;
import com.gabrielcireap.assignment1.dto.builders.UserInfoDtoBuilder;
import com.gabrielcireap.assignment1.entity.Role;
import com.gabrielcireap.assignment1.repository.PatientRepository;
import com.gabrielcireap.assignment1.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CaregiverManagementService {
    private final PatientRepository patientRepository;
    private final UserRepository userRepository;

    @Transactional
    public List<PatientInfoDto> getCaregiversPatients(int caregiverId) {
        return patientRepository.findPatientByCaregiverId(caregiverId)
                .stream().map(PatientInfoDtoBuilder::fromEntity).collect(Collectors.toList());
    }

    @Transactional
    public List<UserInfoDto> findAllCaregivers() {
        return userRepository.findAllByRole(Role.CAREGIVER)
                .stream().map(UserInfoDtoBuilder::fromEntity).collect(Collectors.toList());
    }
}

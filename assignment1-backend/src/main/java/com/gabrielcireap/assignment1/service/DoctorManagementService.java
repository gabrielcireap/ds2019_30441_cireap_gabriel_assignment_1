package com.gabrielcireap.assignment1.service;

import com.gabrielcireap.assignment1.dto.UserInfoDto;
import com.gabrielcireap.assignment1.dto.builders.UserInfoDtoBuilder;
import com.gabrielcireap.assignment1.entity.Role;
import com.gabrielcireap.assignment1.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DoctorManagementService {
    private final UserRepository userRepository;

    @Transactional
    public List<UserInfoDto> findAllDoctors() {
        return userRepository.findAllByRole(Role.DOCTOR)
                .stream().map(UserInfoDtoBuilder::fromEntity).collect(Collectors.toList());
    }
}

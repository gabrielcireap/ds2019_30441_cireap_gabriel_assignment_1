package com.gabrielcireap.assignment1.service;

import com.gabrielcireap.assignment1.dto.MedicationDto;
import com.gabrielcireap.assignment1.dto.MedicationSaveDto;
import com.gabrielcireap.assignment1.dto.builders.MedicationDtoBuilder;
import com.gabrielcireap.assignment1.dto.builders.MedicationSaveDtoBuilder;
import com.gabrielcireap.assignment1.errorhandler.ResourceNotFoundException;
import com.gabrielcireap.assignment1.entity.Medication;
import com.gabrielcireap.assignment1.repository.MedicationRepository;
import com.gabrielcireap.assignment1.validator.MedicationFieldValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MedicationManagementService {
    private final MedicationRepository medicationRepository;

    @Transactional
    public List<MedicationDto> findAll() {
        return medicationRepository.findAll().stream().map(MedicationDtoBuilder::fromEntity).collect(Collectors.toList());
    }

    @Transactional
    public MedicationDto findById(int id) {
        return MedicationDtoBuilder .fromEntity(medicationRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Medication", "id", id)));
    }

    @Transactional
    public void delete(int id) {
        Medication medication = medicationRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Medication", "id", id));
        medicationRepository.delete(medication);
    }

    @Transactional
    public MedicationDto save(MedicationSaveDto medication) {
        MedicationFieldValidator.validateInsertOrUpdate(medication);
        return MedicationDtoBuilder.fromEntity(medicationRepository.save(MedicationSaveDtoBuilder.toEntity(medication)));
    }

    @Transactional
    public MedicationDto update(MedicationDto medicationDto) {
        Medication medication = medicationRepository.findById(medicationDto.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Medication", "id", medicationDto.getId()));
        MedicationFieldValidator.validateInsertOrUpdate(medicationDto);
        medication.setName(medicationDto.getName());
        medication.setSideEffects(medicationDto.getSideEffects());
        return MedicationDtoBuilder.fromEntity(medicationRepository.save(medication));
    }
}

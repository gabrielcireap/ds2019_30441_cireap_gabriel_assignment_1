package com.gabrielcireap.assignment1.service;

import com.gabrielcireap.assignment1.dto.*;
import com.gabrielcireap.assignment1.dto.builders.MedicationPerPlanDtoBuilder;
import com.gabrielcireap.assignment1.dto.builders.MedicationPlanDtoBuilder;
import com.gabrielcireap.assignment1.dto.builders.PatientInfoDtoBuilder;
import com.gabrielcireap.assignment1.dto.builders.PatientSaveDtoBuilder;
import com.gabrielcireap.assignment1.entity.MedicationPlan;
import com.gabrielcireap.assignment1.entity.Patient;
import com.gabrielcireap.assignment1.errorhandler.ResourceNotFoundException;
import com.gabrielcireap.assignment1.repository.MedicationPerPlanRepository;
import com.gabrielcireap.assignment1.repository.MedicationPlanRepository;
import com.gabrielcireap.assignment1.repository.PatientRepository;
import com.gabrielcireap.assignment1.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PatientManagementService {
    private final PatientRepository patientRepository;
    private final UserRepository userRepository;
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationPerPlanRepository medicationPerPlanRepository;
    private final UserManagementService userManagementService;

    @Transactional
    public List<PatientInfoDto> findAll() {
        return patientRepository.findAll().stream().map(PatientInfoDtoBuilder::fromEntity).collect(Collectors.toList());
    }

    @Transactional
    public PatientInfoDto findById(int id) {
        return PatientInfoDtoBuilder.fromEntity(patientRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Patient", "id", id)));
    }

    @Transactional
    public PatientInfoDto findByUserId(int id) {
        return PatientInfoDtoBuilder.fromEntity(patientRepository.findByUserId(id).
                orElseThrow(() -> new ResourceNotFoundException("Patient", "user_id", id)));
    }

    @Transactional
    public void delete(int id) {
        Patient patient = patientRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Patient", "id", id));
        patientRepository.delete(patient);
    }

    @Transactional
    public PatientInfoDto save(PatientSaveDto patientSaveDto) {
        Patient patient = PatientSaveDtoBuilder.toEntity(patientSaveDto);
        patient.setCaregiver(userRepository.findById(patientSaveDto.getCaregiverId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "caregiver-id", patientSaveDto.getCaregiverId())));
        patient.setDoctor(userRepository.findById(patientSaveDto.getDoctorId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "doctor-id", patientSaveDto.getDoctorId())));
        patient.getUser().setId(userRepository.save(patient.getUser()).getId());
        return PatientInfoDtoBuilder.fromEntity(patientRepository.save(patient));
    }

    @Transactional
    public PatientInfoDto update(PatientUpdateDto patientUpdateDto) {
        Patient patient = patientRepository.findById(patientUpdateDto.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Patient", "id", patientUpdateDto.getId()));

        UserUpdateDto userUpdateDto = new UserUpdateDto();
        userUpdateDto.setId(patient.getUser().getId());
        userUpdateDto.setAddress(patientUpdateDto.getAddress());
        userUpdateDto.setEmail(patientUpdateDto.getEmail());
        userUpdateDto.setName(patientUpdateDto.getName());
        userUpdateDto.setPassword(patientUpdateDto.getPassword());
        userManagementService.update(userUpdateDto);

        patient.setMedicalRecord(patientUpdateDto.getMedicalRecord());
        return PatientInfoDtoBuilder.fromEntity(patientRepository.save(patient));
    }

    @Transactional
    public List<MedicationPlanDto> getMedicationPlans(int patientId) {
        List<MedicationPlanDto> medicationPlanDtos = new ArrayList<>();
        List<MedicationPlan> medicationPlans = medicationPlanRepository.getMedicationsPlanByPatientId(patientId);
        for(MedicationPlan plan : medicationPlans) {
            List<MedicationPerPlanDto> medicationPerPlanList = medicationPerPlanRepository.getMedicationPerPlanByMedicationPlanId(plan.getId())
                    .stream().map(MedicationPerPlanDtoBuilder::fromEntity).collect(Collectors.toList());
            medicationPlanDtos.add(MedicationPlanDtoBuilder.fromEntity(plan, medicationPerPlanList));
        }
        return medicationPlanDtos;
    }

    @Transactional
    public List<MedicationPlanDto> getMedicationPlansByUserId(int id) {
        List<MedicationPlanDto> medicationPlanDtos = new ArrayList<>();
        List<MedicationPlan> medicationPlans = medicationPlanRepository.getMedicationsPlanByPatientUsersId(id);
        for(MedicationPlan plan : medicationPlans) {
            List<MedicationPerPlanDto> medicationPerPlanList = medicationPerPlanRepository.getMedicationPerPlanByMedicationPlanId(plan.getId())
                    .stream().map(MedicationPerPlanDtoBuilder::fromEntity).collect(Collectors.toList());
            medicationPlanDtos.add(MedicationPlanDtoBuilder.fromEntity(plan, medicationPerPlanList));
        }
        return medicationPlanDtos;
    }

    @Transactional
    public int saveMedicationPlan(int patientId, MedicationPlanDto medicationPlanDto) {
        Patient patient = patientRepository.findById(patientId).orElseThrow(() -> new ResourceNotFoundException("Patient", "patient-id", patientId));
        MedicationPlan medicationPlan = MedicationPlanDtoBuilder.toEntity(medicationPlanDto, patient);
        medicationPlanRepository.save(medicationPlan);
        medicationPerPlanRepository.saveAll(medicationPlanDto.getMedicationPerPlanDtoList().stream().map((dto) -> MedicationPerPlanDtoBuilder.toEntity(dto, medicationPlan))
                .collect(Collectors.toList()));
        return patientId;
    }
}

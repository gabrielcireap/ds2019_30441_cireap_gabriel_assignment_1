package com.gabrielcireap.assignment1.service;

import com.gabrielcireap.assignment1.dto.UserInfoDto;
import com.gabrielcireap.assignment1.dto.UserLoginDto;
import com.gabrielcireap.assignment1.dto.UserSaveDto;
import com.gabrielcireap.assignment1.dto.UserUpdateDto;
import com.gabrielcireap.assignment1.dto.builders.UserInfoDtoBuilder;
import com.gabrielcireap.assignment1.dto.builders.UserSaveDtoBuilder;
import com.gabrielcireap.assignment1.entity.Role;
import com.gabrielcireap.assignment1.errorhandler.DuplicateUserException;
import com.gabrielcireap.assignment1.errorhandler.ResourceNotFoundException;
import com.gabrielcireap.assignment1.entity.User;
import com.gabrielcireap.assignment1.repository.UserRepository;
import com.gabrielcireap.assignment1.validator.UserFieldValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserManagementService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public List<UserInfoDto> findAll() {
        return userRepository.findAll().stream().map(UserInfoDtoBuilder::fromEntity).collect(Collectors.toList());
    }

    @Transactional
    public UserInfoDto findById(int id) {
        return UserInfoDtoBuilder.fromEntity(userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", id)));
    }

    @Transactional
    public void delete(int id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
        userRepository.delete(user);
    }

    @Transactional
    public UserInfoDto save(UserSaveDto user) {
        UserFieldValidator.validateInsert(user);
        User newUser = UserSaveDtoBuilder.toEntity(user);

        Optional<User> usernameUser = userRepository.findUserByUsername(newUser.getUsername());
        Optional<User> emailUser = userRepository.findUserByEmail(newUser.getEmail());
        if(usernameUser.isPresent() || emailUser.isPresent()) {
            throw new DuplicateUserException();
        } else {
            newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
            return UserInfoDtoBuilder.fromEntity(userRepository.save(newUser));
        }
    }

    @Transactional
    public UserInfoDto update(UserUpdateDto userDto) {
        User user = userRepository.findById(userDto.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userDto.getId()));
        UserFieldValidator.validateUpdate(userDto);
        user.setName(userDto.getName());
        user.setAddress(userDto.getAddress());
        user.setEmail(userDto.getEmail());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        return UserInfoDtoBuilder.fromEntity(userRepository.save(user));
    }

    @Transactional
    public UserInfoDto login(UserLoginDto userLoginDto) {
        User user = userRepository.findUserByUsername(userLoginDto.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", userLoginDto.getUsername()));
        return UserInfoDtoBuilder.fromEntity(user);
    }

    @Transactional
    public List<User> findByRole(Role role) {
        return userRepository.findAllByRole(role);
    }
}

package com.gabrielcireap.assignment1.validator;

public interface FieldValidator<T> {
    boolean validate(T t);
}
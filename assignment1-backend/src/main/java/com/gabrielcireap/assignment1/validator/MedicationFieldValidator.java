package com.gabrielcireap.assignment1.validator;

import com.gabrielcireap.assignment1.dto.MedicationDto;
import com.gabrielcireap.assignment1.dto.MedicationSaveDto;
import com.gabrielcireap.assignment1.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class MedicationFieldValidator {
    private static final Log LOGGER = LogFactory.getLog(MedicationFieldValidator.class);

    public static void validateInsertOrUpdate(MedicationSaveDto medicationSaveDto) {
        MedicationDto medicationDto = new MedicationDto();
        medicationDto.setName(medicationSaveDto.getName().trim());
        medicationDto.setSideEffects(medicationSaveDto.getSideEffects().trim());
        medicationDto.setId(null);
        validateInsertOrUpdate(medicationDto);
    }

    public static void validateInsertOrUpdate(MedicationDto medicationDto) {
        List<String> errors = new ArrayList<>();
        if (medicationDto == null) {
            errors.add("medicationDto is null");
            throw new IncorrectParameterException(MedicationDto.class.getSimpleName(), errors);
        }

        if(medicationDto.getName() == null || medicationDto.getName().equals("")) {
            errors.add("medicationDto's name is null");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(MedicationFieldValidator.class.getSimpleName(), errors);
        }
    }

    
}

package com.gabrielcireap.assignment1.validator;
import com.gabrielcireap.assignment1.dto.UserSaveDto;
import com.gabrielcireap.assignment1.dto.UserUpdateDto;
import com.gabrielcireap.assignment1.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class UserFieldValidator{

    private static final Log LOGGER = LogFactory.getLog(UserFieldValidator.class);
    private static final EmailFieldValidator EMAIL_VALIDATOR = new EmailFieldValidator() ;

    public static void validateInsert(UserSaveDto userSaveDto) {
        List<String> errors = new ArrayList<>();
        if (userSaveDto == null) {
            errors.add("userSaveDto is null");
            throw new IncorrectParameterException(UserSaveDto.class.getSimpleName(), errors);
        }

        if (userSaveDto.getEmail() == null || !EMAIL_VALIDATOR.validate(userSaveDto.getEmail())) {
            errors.add("User email has invalid format");
        }

        if (userSaveDto.getUsername() == null || userSaveDto.getUsername().equals("")) {
            errors.add("User's username is null");
        }

        if (userSaveDto.getPassword() == null || userSaveDto.getPassword().equals("")) {
            errors.add("User's password is null");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(UserFieldValidator.class.getSimpleName(), errors);
        }
    }

    public static void validateUpdate(UserUpdateDto userUpdateDto) {
        List<String> errors = new ArrayList<>();
        if (userUpdateDto == null) {
            errors.add("userUpdateDto is null");
            throw new IncorrectParameterException(UserUpdateDto.class.getSimpleName(), errors);
        }

        if (userUpdateDto.getAddress() == null || userUpdateDto.getAddress().equals("")) {
            errors.add("User's address is not valid");
        }

        if (userUpdateDto.getName() == null || userUpdateDto.getName().equals("")) {
            errors.add("User's name is not valid");
        }

        if (userUpdateDto.getId() == null) {
            errors.add("User's id is null");
        }

        if (userUpdateDto.getEmail() == null || !EMAIL_VALIDATOR.validate(userUpdateDto.getEmail())) {
            errors.add("User's email is not valid");
        }

        if (userUpdateDto.getPassword() == null || userUpdateDto.getPassword().length() < 6) {
            errors.add("User's password is not valid");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(UserFieldValidator.class.getSimpleName(), errors);
        }
    }
}
import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import Persons from './person-data/person/persons'
import Medications from "./medication-data/medication/medications";
import CaregiverPage from "./caregiver-data/caregiver/caregiver-page";
import Patients from "./patient-data/patient/patients";
import MedicationPlans from "./patient-data/patient/medication-plans";
import MedicationPlansCaregiver from "./caregiver-data/caregiver/medication-plans";
import LoginForm from "./home/login-form";
import PatientProfile from "./patient-data/patient/profile";

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';

let enums = require('./commons/constants/enums');

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/staff'
                            render={() => <Persons/>}
                        />

						<Route
                            exact
                            path='/medications'
                            render={() => <Medications/>}
                        />

						<Route
                            exact
                            path='/caregivers/page'
                            render={() => <CaregiverPage/>}
                        />

                        <Route
                            exact
                            path='/caregivers/medication-plans'
                            render={() => <MedicationPlansCaregiver />}
                        />

						<Route
                            exact
                            path='/patients'
                            render={() => <Patients/>}
                        />

                        <Route
                            exact
                            path='/medication-plans'
                            render={() => <MedicationPlans />}
                        />

                        <Route
                            exact
                            path='/profile'
                            render={() => <PatientProfile />}
                        />

						<Route
                            exact
                            path='/login'
                            render={() => <LoginForm/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App

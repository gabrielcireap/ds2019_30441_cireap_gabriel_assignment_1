import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_persons: '/caregiver'
};

function getPersons(caregiverId, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_persons + "/" + caregiverId + "/patients", {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
		}
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
	getPersons
}
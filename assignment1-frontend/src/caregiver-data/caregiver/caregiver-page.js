import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"

import * as API_CAREGIVERS from "./api/caregiver-api"

const columns = [
	{
        Header:  'Patient ID',
        accessor: 'id'
    },
    {
        Header:  'Name',
        accessor: 'name'
    },
	{
        Header:  'Address',
        accessor: 'address'
    },
	{
        Header:  'Birth Date',
        accessor: 'birthDate'
    },
	{
        Header:  'Gender',
        accessor: 'gender'
    },
	{
        Header: 'Email',
        accessor: 'email'
    },
    {
        Header: 'Doctor',
        accessor: 'doctor'
    },
    {
        Header: 'Medical Record',
        accessor: 'medicalRecord'
    }
];

const filters = [
    {
        accessor: 'name'
    },
    {
        accessor: 'address'
    },
    {
        accessor: 'birth date'
    },
    {
        accessor: 'gender'
    },
    {
        accessor: 'email'
    },
    {
        accessor: 'doctor'
    },
    {
        accessor: 'medicalRecord'
    }
];

class CaregiverPage extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchPersons();
    }

    componentWillMount() {
        if (sessionStorage.getItem("Role") !== "CAREGIVER") {
            window.location.assign("/")
        }
    }

    fetchPersons() {
        return API_CAREGIVERS.getPersons(sessionStorage.getItem('Id'), (result, status, err) => {
            console.log(result);
			if(result !== null && status === 200) {
               result.forEach( x => {
                   this.tableData.push({
                       id: x.id,
                       name: x.user.name,
                       address: x.user.address,
                       birthDate: x.user.birthDate,
                       gender: x.user.gender,
                       email: x.user.email,
                       doctor: x.doctor.name,
                       medicalRecord: x.medicalRecord
                   });
               });
               this.forceUpdate();
			} else {
               console.log("Am prins o eroare!!!");
			   this.state = {
			   	   ...this.state,
				   errorStatus: status,
				   error: err
			   }
               this.forceUpdate();
			}
        });
    }

    refresh() {
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default CaregiverPage;

import React from 'react';
import validate from "../../person-data/person/validators/person-validators";
import "../../person-data/person/fields/fields.css";
import Button from "react-bootstrap/Button";
import * as API_CAREGIVERS from "./api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";


class MedicationPlanForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,
            prescriptionIsValid: false,
            didAddPrescription: false,
            registerForm: props.register,

            patients: [],

            formControls: {
                patient: {
                    value: '',
                    touched: false
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchPatients = this.fetchPatients.bind(this);
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    componentDidMount() {
        this.fetchPatients()
    }

    fetchPatients() {
        return API_CAREGIVERS.getPersons(sessionStorage.getItem("Id"), (result, status, err) => {
            console.log(result);
            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.state.patients.push(x);
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;
        let formIsValid = updatedControls["patient"].touched;
        let prescriptionIsValid = true;

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid,
            prescriptionIsValid: prescriptionIsValid
        });
    };

    handleSubmit() {
        this.props.handleSubmit(this.state.formControls.patient.value)
    }
    
    render() {
        return (
            <form id="medicationPlanForm">

                <h1>View medication plans</h1>
                
                <p> Patient: </p>
                <select name="patient" form="medicationPlanForm" onChange={this.handleChange}>
                    <option value="" selected disabled hidden> Choose a patient </option>
                    {this.state.patients.map((patient, index) => (
                        <option value={patient.id}> {patient.user.name} </option>
                    ))}
                </select>

                <p></p>
                <Button variant="success"
                    type={"submit"}
                    disabled={!this.state.formIsValid}
                    onClick={this.handleSubmit}>
                    Submit
                </Button>

                {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />}

            </form>

        );
    }
}

export default MedicationPlanForm;

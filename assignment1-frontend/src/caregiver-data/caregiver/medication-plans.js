import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import { Card, Col, Row } from 'reactstrap';
import Table from "../../commons/tables/table"
import MedicationPlanForm from "./medication-plan-form";

import * as API_PATIENTS from "../../patient-data/patient/api/patient-api"

const columnsMedicationPlans = [
    {
        Header: 'Medication ID',
        accessor: 'id'
    },
    {
        Header: 'Medication',
        accessor: 'name'
    },
    {
        Header: 'Side Effects',
        accessor: 'sideEffects'
    },
    {
        Header: 'Dosage',
        accessor: 'dosage'
    },
    {
        Header: 'Intake Moments',
        accessor: 'intakeMoments'
    },
    {
        Header: 'Start Date',
        accessor: 'startDate'
    },
    {
        Header: 'End Date',
        accessor: 'endDate'
    }
];

const filtersMedicationPlans = [
    {
        accessor: 'medication'
    },
    {
        accessor: 'sideEffects'
    },
    {
        accessor: 'dosage'
    },
    {
        accessor: 'intakeMoments'
    },
    {
        accessor: 'startDate'
    },
    {
        accessor: 'endDate'
    }
];

class MedicationPlans extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null,
            action: ""
        };
        
        this.tableDataPlans = [];
        this.handleSubmit = this.handleSubmit.bind(this)
        this.fetchMedicationPlan = this.fetchMedicationPlan.bind(this)
    }

    componentWillMount() {
        if (sessionStorage.getItem("Role") !== "CAREGIVER") {
            window.location.assign("/")
        }

        let plansData = sessionStorage.getItem("plans")
        this.tableDataPlans = plansData === "null" ? [] : JSON.parse(plansData)
        debugger;
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    fetchMedicationPlan(patientId) {
        debugger;
        return API_PATIENTS.getMedicationPlan(patientId, (result, status, err) => {
            console.log(result);
            debugger;
            if (result !== null && status >= 200 && status < 300) {
                let plans = [];
                result.forEach(medicationPlan => {
                    medicationPlan.medicationPerPlanDtoList.forEach(plan => {
                        plans.push({
                            id: plan.medicationDto.id,
                            name: plan.medicationDto.name,
                            sideEffects: plan.medicationDto.sideEffects,
                            dosage: plan.dosage,
                            intakeMoments: plan.intakeMoments,
                            startDate: medicationPlan.startTime,
                            endDate: medicationPlan.endTime
                        });
                    })
                });
                sessionStorage.setItem("plans", JSON.stringify(plans))
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                debugger;
                this.forceUpdate();
            }
        });
    }

    handleSubmit(patientId) {
        this.fetchMedicationPlan(patientId)
    }

    refresh() {
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableDataPlans}
                                columns={columnsMedicationPlans}
                                search={filtersMedicationPlans}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <MedicationPlanForm
                                    actionChanged={this.actionChanged}
                                    handleSubmit={this.handleSubmit}
                                >
                                </MedicationPlanForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />}

            </div>
        );
    };

}

export default MedicationPlans;

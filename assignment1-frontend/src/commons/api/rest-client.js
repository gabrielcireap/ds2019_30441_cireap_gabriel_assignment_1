function performRequest(request, callback) {
    fetch(request)
        .then(
            function (response) {
                if (response.ok) {
                    response.json().then(json => callback(json, response.status, null));
                }
                else if (response.status !== 401) {
                    response.json().then(err => callback(null, response.status, err));
                } else {
                    callback(null, response.status, null)
                }
            })
        .catch(function (err) {
            //catch any other unexpected error, and set custom code for error = 1
            debugger;
            callback(null, 1, err)
        });
}

module.exports = {
    performRequest
};

import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Container, Jumbotron} from 'reactstrap';

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class Home extends React.Component {
    
	logout() {
		sessionStorage.setItem("Username", null)
        sessionStorage.setItem("Password", null)
        sessionStorage.setItem("Role", null)
        sessionStorage.setItem("plans", null)
        sessionStorage.setItem("prescriptions", null)
        window.location.assign("/")
    }

    render() {

        return (

            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Integrated Medical Monitoring Platform for Home-care assistance</h1>
                        <p className="lead" style={textStyle}> <b>Enabling real time monitoring of patients, remote-assisted care services and
                            smart intake mechanism for prescribed medication.</b> </p>
                        <hr className="my-2"/>
                        <p  style={textStyle}> <b>This assignment represents the first module of the distributed software system "Integrated
                            Medical Monitoring Platform for Home-care assistance that represents the final project
                            for the Distributed Systems course. </b> </p>
                        <p className="lead">
                            <Button color="primary" onClick={() => window.open('http://coned.utcluj.ro/~salomie/DS_Lic/')}>Learn
                                More</Button>
                        </p>

						{
                            sessionStorage.getItem("Username") == "null"
                                || sessionStorage.getItem("Password") == "null"
                                || sessionStorage.getItem("Role") == "null" ?
						<div>
							<Button color="primary" onClick={() => window.location.assign("/login")}> Login </Button>
						</div>
						:
						<div>
							<Button color="primary" onClick={this.logout}> Logout </Button>
						</div>
						}
                        
                    </Container>
                </Jumbotron>

            </div>
        )
    };
}

export default Home

import React from 'react';
import validate from "../person-data/person/validators/person-validators";
import TextInput from "../person-data/person/fields/TextInput";
import PasswordInput from "../person-data/person/fields/PasswordInput";
import '../person-data/person/fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "../person-data/person/api/person-api";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";

class LoginForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {
            errorStatus: 0,
            error: null,
            formIsValid: false,
            formControls : {
               username: {
                   value: '',
                   placeholder: 'Enter username...',
                   valid: false,
                   touched: false, 
				   validationRules: {
				   		isRequired: true
				   }
               },

               password: {
                   value: '',
                   placeholder: 'Enter password...',
                   valid: false,
                   touched: false,
                   validationRules: {
					   minLength: 6,
					   isRequired: true
                   }
               }               
			}
        };

        this.handleChange = this.handleChange.bind(this);
		this.handleLogin = this.handleLogin.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
		
    }

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

	login(user) {
        return API_USERS.login(user, (result, status, error) => {
            console.log(result);
            if(result !== null && (status >= 200 && status < 300)) {
				sessionStorage.setItem('Username', user.username)
				sessionStorage.setItem('Password', user.password)
                sessionStorage.setItem('Role', result.role)
                sessionStorage.setItem('Id', result.id)
				window.location.assign("/")
                this.props.refresh();
            } else {
				window.location.assign("/login")
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

	handleLogin() {
        let user = {
            username: this.state.formControls.username.value.trim(),
            password : this.state.formControls.password.value.trim(),
        };
        this.login(user);
	}

    render() {
        return (

          <div>

              <h1>Login</h1>

			  <p> Userame: </p>

              <TextInput name="username"
                         placeholder={this.state.formControls.username.placeholder}
                         value={this.state.formControls.username.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.username.touched}
                         valid={this.state.formControls.username.valid}
              />

              <p> Password: </p>
			  
			  <PasswordInput name="password"
                         placeholder={this.state.formControls.password.placeholder}
                         value={this.state.formControls.password.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.password.touched}
                         valid={this.state.formControls.password.valid}
              />

              {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
              <div className={"error-message row"}> * Password must have at least 6 characters </div>}

              <p></p>
              <Button variant="success"
                      type={"submit"}
                      disabled={!this.state.formIsValid}
					  onClick={this.handleLogin}>
                  Login
              </Button>
          </div>

        );
    }
}

export default LoginForm;
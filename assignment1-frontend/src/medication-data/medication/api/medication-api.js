import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_medications: "/medications",
    post_medication: "/medications",
	put_medication: "/medications",
	delete_medication: "/medications"
};

function getMedications(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medications, {
        method: 'GET',
		headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        }
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback)
}

function getMedicationById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.get_medications + "/" + params.id, {
       method: 'GET',
	   headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
		}
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedication(medication, callback){
    let request = new Request(HOST.backend_api + endpoint.post_medication , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        },
        body: JSON.stringify(medication)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function editMedication(medication, callback){
	
	console.log(HOST.backend_api + endpoint.put_medication)

    let request = new Request(HOST.backend_api + endpoint.put_medication , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        },
        body: JSON.stringify(medication)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteMedication(medicationId, callback){
    let request = new Request(HOST.backend_api + endpoint.delete_medication + "/" + medicationId, {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        }
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getMedications,
    getMedicationById,
    postMedication,
	editMedication,
	deleteMedication
};

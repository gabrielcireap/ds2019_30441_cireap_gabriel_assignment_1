import React from 'react';
import validate from "../../person-data/person/validators/person-validators";
import TextInput from "../../person-data/person/fields/TextInput";
import '../../person-data/person/fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_MEDICATIONS from "./api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";

class MedicationDeleteForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {
            errorStatus: 0,
            error: null,
            formIsValid: false,
            formControls : {

				id: {
                   value: '',
                   placeholder: 'What is the id of the medication?...',
                   valid: false,
                   touched: false,
                   validationRules: {
                       isRequired: true
                   }
               }           
           }
        };

        this.handleChange = this.handleChange.bind(this);
		this.handleDelete = this.handleDelete.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
		
    }

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

	deleteMedication(medicationId){
        return API_MEDICATIONS.deleteMedication(medicationId, (result, status, error) => {
            console.log(result);

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully deleted medication with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

	handleDelete() {
        this.deleteMedication(this.state.formControls.id.value);
	}

    render() {
        return (

          <form onSubmit={this.handleDelete}>

              <h1>Delete medication</h1>

			  <p> ID: </p>

              <TextInput name="id"
                         placeholder={this.state.formControls.id.placeholder}
                         value={this.state.formControls.id.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.id.touched}
                         valid={this.state.formControls.id.valid}
              />
              {this.state.formControls.id.touched && !this.state.formControls.id.valid &&
              <div className={"error-message row"}> * ID must have at least 1 characters </div>}

              <p></p>
              <Button variant="success"
                      type={"delete"}
                      disabled={!this.state.formIsValid}>
                  Delete
              </Button>

              {this.state.errorStatus > 0 &&
              <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

          </form>

        );
    }
}

export default MedicationDeleteForm;

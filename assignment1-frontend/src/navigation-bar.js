import React from 'react'
import logo from './commons/images/icon.png';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBar = () => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>
			{
				sessionStorage.getItem("Username") !== "null" &&
                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                       Menu
                    </DropdownToggle>
                    <DropdownMenu right >
                        <DropdownItem>
							{
							sessionStorage.getItem("Role") === "DOCTOR" ?
							<div>
								<NavLink href="/staff">Staff</NavLink>
								<NavLink href="/medications">Medications</NavLink>
								<NavLink href="/patients">Patients</NavLink>
								<NavLink href="/medication-plans">Medication Plans</NavLink>
							</div>	:
                            sessionStorage.getItem("Role") === "CAREGIVER" ?
                            <div>
								<NavLink href="/caregivers/page">Caregiver Page</NavLink>
                                <NavLink href="/caregivers/medication-plans">Medication Plans</NavLink>
                            </div>
							:
								<NavLink href="/profile">View Profile</NavLink>
							}
                        </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
			}
            </Nav>
        </Navbar>
    </div>
);

export default NavigationBar

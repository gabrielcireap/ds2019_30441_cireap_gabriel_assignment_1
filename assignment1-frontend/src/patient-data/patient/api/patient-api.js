import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_patients: '/patients',
    get_doctors: '/doctors',
    get_caregivers: '/caregivers',
    post_patient: "/patients",
	update_patient: "/patients",
    delete_patient: "/patients",
    add_medication_plan: "/patients"
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients, {
        method: 'GET',
		headers: {
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
		}
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientById(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients + "/" + id, {
        method: 'GET',
        headers: {
            'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        }
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPatient(patient, callback){
    let request = new Request(HOST.backend_api + endpoint.post_patient , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updatePatient(patient, callback){
    let request = new Request(HOST.backend_api + endpoint.update_patient , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        },
        body: JSON.stringify(patient)
    });
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletePatient(patientId, callback){
    let request = new Request(HOST.backend_api + endpoint.delete_patient + "/" + patientId, {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        }
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getDoctors(callback){
    let request = new Request(HOST.backend_api + endpoint.get_doctors, {
        method: 'GET',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        }
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregivers(callback){
    let request = new Request(HOST.backend_api + endpoint.get_caregivers, {
        method: 'GET',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        }
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function addMedicationPlan(patientId, medicationPlan, callback) {
    debugger;
    let request = new Request(HOST.backend_api + endpoint.add_medication_plan + "/" + patientId + "/plan", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        },
        body: JSON.stringify(medicationPlan)
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationPlan(patientId, callback) {
    let request = new Request(HOST.backend_api + endpoint.add_medication_plan + "/" + patientId + "/plan", {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        }
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationPlanByUserId(userId, callback) {
    let request = new Request(HOST.backend_api + endpoint.add_medication_plan + "/" + userId + "/plan/user", {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        }
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}


export {
    getPatients,
    getPatientById,
    postPatient,
	updatePatient,
	deletePatient,
	getDoctors,
    getCaregivers,
    addMedicationPlan,
    getMedicationPlan,
    getMedicationPlanByUserId
};

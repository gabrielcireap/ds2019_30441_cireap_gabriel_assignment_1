import React from 'react';
import validate from "../../person-data/person/validators/person-validators";
import TextInput from "../../person-data/person/fields/TextInput";
import "../../person-data/person/fields/fields.css";
import Button from "react-bootstrap/Button";
import * as API_PATIENTS from "./api/patient-api";
import * as API_MEDICATIONS from "../../medication-data/medication/api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";


class MedicationPlanForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,
            prescriptionIsValid: false,
            didAddPrescription: false,
            registerForm: props.register,

            patients: [],
            medications: [],

            action: {
                value: "READ",
                valid: true,
                touched: true
            },

            formControls: {

                dosage: {
                    value: '',
                    placeholder: 'What is the dosage?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },

                intakeMoments: {
                    value: '',
                    placeholder: 'What are the intake moments?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },

                startDate: {
                    value: '',
                    placeholder: 'What is the start date of the plan?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },

                endDate: {
                    value: '',
                    placeholder: 'What is the end date of the plan?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },

                medication: {
                    value: '',
                    touched: false
                },

                patient: {
                    value: '',
                    touched: false
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAddPrescription = this.handleAddPrescription.bind(this);
        this.fetchMedications = this.fetchMedications.bind(this);
        this.fetchPatients = this.fetchPatients.bind(this);
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    componentDidMount() {
        this.fetchPatients()
        this.fetchMedications()
    }

    fetchPatients() {
        return API_PATIENTS.getPatients((result, status, err) => {
            console.log(result);
            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.state.patients.push(x);
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    fetchMedications() {
        return API_MEDICATIONS.getMedications((result, status, err) => {
            console.log(result);

            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.state.medications.push(x);
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        })
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        if (name === "action") {
            this.state.action.value = value;
            this.props.actionChanged(value);
            return;
        }

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;
        let formIsValid = true;
        let prescriptionIsValid = true;

        switch (this.state.action.value) {
            case "INSERT":
                formIsValid = updatedControls["startDate"].valid && updatedControls["endDate"].valid && updatedControls["patient"].touched
                prescriptionIsValid = updatedControls["medication"].touched && updatedControls["dosage"].valid
                    && updatedControls["intakeMoments"].valid
                break;
            default:
                formIsValid = updatedControls["patient"].touched
                break;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid,
            prescriptionIsValid: prescriptionIsValid
        });
    };

    parse(string) {
        let stringArr = string.split(" ")
        return {
            id: parseInt(stringArr[0]),
            name: stringArr[1],
            sideEffects: stringArr[2]
        }
    }

    handleAddPrescription() {
        this.state.didAddPrescription = true
        this.props.handleAddPrescription({
            medication: this.parse(this.state.formControls.medication.value),
            dosage: this.state.formControls.dosage.value,
            intakeMoments: this.state.formControls.intakeMoments.value
        });
    }

    handleSubmit() {
        this.props.handleSubmit({
            action: this.state.action.value,
            patientId: this.state.formControls.patient.value,
            startDate: this.state.formControls.startDate.value,
            endDate: this.state.formControls.endDate.value
        })
    }
    
    render() {
        return (
            <form id="medicationPlanForm">

                <h1> Select action </h1>
                <select name="action" form="medicationPlanForm" onChange={this.handleChange}>
                    <option value="READ"> Read </option>
                    <option value="INSERT"> Insert </option>
                </select>

                <h1>{this.state.action.value} medication plan</h1>
                
                {
                    this.state.action.value === "INSERT" &&
                    <div>
                        <p> Medication: </p>
                        <select name="medication" form="medicationPlanForm" onChange={this.handleChange}>
                            <option value="" selected disabled hidden> Choose a medication </option>
                            {this.state.medications.map((medication, index) => (
                                <option value={[medication.id, medication.name, medication.sideEffects].join(" ")}> {medication.name} </option>
                            ))}
                        </select>
                    </div>
                }

                {
                    this.state.action.value === "INSERT" &&
                    <div>
                        <p> Dosage: </p>
                        <TextInput name="dosage"
                            placeholder={this.state.formControls.dosage.placeholder}
                            value={this.state.formControls.dosage.value}
                            onChange={this.handleChange}
                            touched={this.state.formControls.dosage.touched}
                            valid={this.state.formControls.dosage.valid}
                        />
                        {this.state.formControls.dosage.touched && !this.state.formControls.dosage.valid &&
                        <div className={"error-message row"}> * Dosage is required </div>}
                    </div>
                }

                {
                    this.state.action.value === "INSERT" &&
                    <div>
                        <p> Intake Moments: </p>
                        <TextInput name="intakeMoments"
                            placeholder={this.state.formControls.intakeMoments.placeholder}
                            value={this.state.formControls.intakeMoments.value}
                            onChange={this.handleChange}
                            touched={this.state.formControls.intakeMoments.touched}
                            valid={this.state.formControls.intakeMoments.valid}
                        />
                        {this.state.formControls.intakeMoments.touched && !this.state.formControls.intakeMoments.valid &&
                            <div className={"error-message row"}> * Intake Moments is required </div>}
                    </div>
                }

                {
                    this.state.action.value === "INSERT" &&
                    <div>
                        <Button variant="success"
                            type={"submit"}
                            disabled={!this.state.prescriptionIsValid}
                            onClick={this.handleAddPrescription}>

                            Add Prescription
                        </Button>
                    </div>
                }

                {
                    this.state.action.value === "INSERT" &&
                    <div>
                        <p> Start Date: </p>
                        <TextInput name="startDate"
                            placeholder={this.state.formControls.startDate.placeholder}
                            value={this.state.formControls.startDate.value}
                            onChange={this.handleChange}
                            touched={this.state.formControls.startDate.touched}
                            valid={this.state.formControls.startDate.valid}
                        />
                        {this.state.formControls.startDate.touched && !this.state.formControls.startDate.valid &&
                            <div className={"error-message row"}> * Start Date is required </div>}
                    </div>
                }

                {
                    this.state.action.value === "INSERT" &&
                    <div>
                        <p> End Date: </p>

                        <TextInput name="endDate"
                            placeholder={this.state.formControls.endDate.placeholder}
                            value={this.state.formControls.endDate.value}
                            onChange={this.handleChange}
                            touched={this.state.formControls.endDate.touched}
                            valid={this.state.formControls.endDate.valid}
                        />
                        {this.state.formControls.endDate.touched && !this.state.formControls.endDate.valid &&
                            <div className={"error-message row"}> * End Date is required </div>}
                    </div>
                }

                <p> Patient: </p>
                <select name="patient" form="medicationPlanForm" onChange={this.handleChange}>
                    <option value="" selected disabled hidden> Choose a patient </option>
                    {this.state.patients.map((patient, index) => (
                        <option value={patient.id}> {patient.user.name} </option>
                    ))}
                </select>

                <p></p>
                <Button variant="success"
                    type={"submit"}
                    disabled={!this.state.formIsValid}
                    onClick={this.handleSubmit}>
                    Submit
                </Button>

                {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />}

            </form>

        );
    }
}

export default MedicationPlanForm;

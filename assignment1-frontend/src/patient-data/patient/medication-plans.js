import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import { Card, Col, Row } from 'reactstrap';
import Table from "../../commons/tables/table"
import MedicationPlanForm from "./medication-plan-form";

import * as API_PATIENTS from "./api/patient-api"

const columnsPrescription = [
    {
        Header: 'Medication',
        accessor: 'medication'
    },
    {
        Header: 'Dosage',
        accessor: 'dosage'
    },
    {
        Header: 'Intake Moments',
        accessor: 'intakeMoments'
    }
];

const columnsMedicationPlans = [
    {
        Header: 'Medication ID',
        accessor: 'id'
    },
    {
        Header: 'Medication',
        accessor: 'name'
    },
    {
        Header: 'Side Effects',
        accessor: 'sideEffects'
    },
    {
        Header: 'Dosage',
        accessor: 'dosage'
    },
    {
        Header: 'Intake Moments',
        accessor: 'intakeMoments'
    },
    {
        Header: 'Start Date',
        accessor: 'startDate'
    },
    {
        Header: 'End Date',
        accessor: 'endDate'
    }
];

const filtersPrescription = [
    {
        accessor: 'medication'
    },
    {
        accessor: 'dosage'
    },
    {
        accessor: 'intakeMoments'
    }
];

const filtersMedicationPlans = [
    {
        accessor: 'medication'
    },
    {
        accessor: 'sideEffects'
    },
    {
        accessor: 'dosage'
    },
    {
        accessor: 'intakeMoments'
    },
    {
        accessor: 'startDate'
    },
    {
        accessor: 'endDate'
    }
];

class MedicationPlans extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null,
            action: "READ"
        };

        this.tableDataPrescriptions = [];
        this.tableDataPlans = [];
        this.actionChanged = this.actionChanged.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleAddPrescription = this.handleAddPrescription.bind(this)
        this.fetchMedicationPlan = this.fetchMedicationPlan.bind(this)
    }

    componentWillMount() {
        if (sessionStorage.getItem("Role") !== "DOCTOR") {
            window.alert("Access denied!")
            window.location.assign("/")
        }

        let prescriptionData = sessionStorage.getItem("prescriptions")
        this.tableDataPrescriptions = prescriptionData === "null" ? [] : JSON.parse(prescriptionData)
        let plansData = sessionStorage.getItem("plans")
        this.tableDataPlans = plansData === "null" ? [] : JSON.parse(plansData)
        debugger;
        //this.actionChanged("READ")
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    fetchMedicationPlan(patientId) {
        debugger;
        return API_PATIENTS.getMedicationPlan(patientId, (result, status, err) => {
            console.log(result);
            debugger;
            if (result !== null && status >= 200 && status < 300) {
                let plans = [];
                result.forEach(medicationPlan => {
                    medicationPlan.medicationPerPlanDtoList.forEach(plan => {
                        plans.push({
                            id: plan.medicationDto.id,
                            name: plan.medicationDto.name,
                            sideEffects: plan.medicationDto.sideEffects,
                            dosage: plan.dosage,
                            intakeMoments: plan.intakeMoments,
                            startDate: medicationPlan.startTime,
                            endDate: medicationPlan.endTime
                        });
                    })
                });
                sessionStorage.setItem("plans", JSON.stringify(plans))
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    addMedicationPlan(medicationPlan) {
        debugger;
        return API_PATIENTS.addMedicationPlan(medicationPlan.patientId, medicationPlan.plan, (result, status, err) => {
            console.log(result);
            debugger;
            if (result !== null && status === 200) {
                sessionStorage.removeItem("prescriptions")
                this.forceUpdate();
            } else {
                debugger;
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    actionChanged(value) {
        this.state.action = value
        this.refresh();
    }

    handleAddPrescription(prescription) {
        this.tableDataPrescriptions.push({
            id: prescription.medication.id,
            medication: prescription.medication.name,
            sideEffects: prescription.medication.sideEffects,
            dosage: prescription.dosage,
            intakeMoments: prescription.intakeMoments
        })
        sessionStorage.setItem("prescriptions", JSON.stringify(this.tableDataPrescriptions))
        this.refresh()
    }

    handleSubmit(medicationPlan) {
        if (medicationPlan.action === "INSERT") {
            let medicationList = [];
            this.tableDataPrescriptions.forEach(data => (
                medicationList.push({
                    id: undefined,
                    medicationDto: {
                        id: data.id,
                        name: data.medication,
                        sideEffects: data.sideEffects
                    },
                    dosage: parseInt(data.dosage),
                    intakeMoments: data.intakeMoments
                })
            ))
            
            this.addMedicationPlan({
                patientId: medicationPlan.patientId,
                plan: {
                    id: undefined,
                    startTime: medicationPlan.startDate,
                    endTime: medicationPlan.endDate,
                    medicationPerPlanDtoList: medicationList
                }
            });
        } else if (medicationPlan.action === "READ") {
            this.fetchMedicationPlan(medicationPlan.patientId)
        }
    }

    refresh() {
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            {this.state.action === "INSERT" &&
                                <Table
                                    data={this.tableDataPrescriptions}
                                    columns={columnsPrescription}
                                    search={filtersPrescription}
                                    pageSize={pageSize}
                                />
                            }

                            {this.state.action === "READ" &&
                                <Table
                                    data={this.tableDataPlans}
                                    columns={columnsMedicationPlans}
                                    search={filtersMedicationPlans}
                                    pageSize={pageSize}
                                />
                            }

                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <MedicationPlanForm
                                    actionChanged={this.actionChanged}
                                    handleAddPrescription={this.handleAddPrescription}
                                    handleSubmit={this.handleSubmit}
                                >
                                </MedicationPlanForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />}

            </div>
        );
    };

}

export default MedicationPlans;

import React from 'react';
import validate from "../../person-data/person/validators/person-validators";
import TextInput from "../../person-data/person/fields/TextInput";
import PasswordInput from "../../person-data/person/fields/PasswordInput";
import "../../person-data/person/fields/fields.css";
import Button from "react-bootstrap/Button";
import * as API_PATIENTS from "./api/patient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";


class PatientForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,
            registerForm: props.register,

            doctors: [],
            caregivers: [],

            formControls: {

                action: {
                    value: 'INSERT',
                    touched: true,
                    valid: true
                },

                id: {
                    value: undefined,
                    placeholder: 'What is the ID of the user?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 1,
                        isRequired: true
                    }
                },

                name: {
                    value: '',
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                address: {
                    value: '',
                    placeholder: 'What is your address?...',
                    valid: false,
                    touched: false,
                },

                birthDate: {
                    value: '',
                    placeholder: 'What is your birth date?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        birthDateValidator: true
                    }
                },

                username: {
                    value: '',
                    placeholder: 'Username...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },

                password: {
                    value: '',
                    placeholder: 'Password...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 6,
                        isRequired: true
                    }
                },

                email: {
                    value: '',
                    placeholder: 'Email...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        emailValidator: true
                    }
                },

                gender: {
                    value: '',
                    placeholder: 'Gender...',
                    valid: true,
                    touched: true
                },

                role: {
                    value: 'PATIENT',
                    valid: true,
                    touched: true
                },

                medicalRecord: {
                    value: '',
                    placeholder: "Enter medical record here...",
                    valid: false,
                    touched: false
                },

                doctor: {
                    value: '',
                    touched: false
                },

                caregiver: {
                    value: '',
                    touched: false
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    componentDidMount() {
        this.fetchDoctors()
        this.fetchCaregivers()
    }

    fetchDoctors() {
        return API_PATIENTS.getDoctors((result, status, err) => {
            console.log(result);

            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.state.doctors.push(x);
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    fetchCaregivers() {
        return API_PATIENTS.getCaregivers((result, status, err) => {
            console.log(result);

            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.state.caregivers.push(x);
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;
        let formIsValid = true;

        switch (this.state.formControls.action.value) {
            case "INSERT":
                for (let updatedFormElementName in updatedControls) {
                    if (updatedFormElementName !== "id") {
                        formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
                    }
                }
                break;
            case "DELETE":
                formIsValid = updatedControls["id"].valid
                break;
            default:
                formIsValid = updatedControls["id"].valid && updatedControls["name"].valid && updatedControls["address"].valid
                    && updatedControls["password"].valid && updatedControls["email"].valid &&
                    updatedControls["medicalRecord"].valid
                break;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerPatient(patient) {
        debugger;
        return API_PATIENTS.postPatient(patient, (result, status, error) => {
            console.log(result);

            if (result !== null && (status >= 200 && status < 300)) {
                console.log("Successfully inserted person: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    deletePatient(patientId) {
        return API_PATIENTS.deletePatient(patientId, (result, status, error) => {
            console.log(result);

            if (result !== null && (status >= 200 && status < 300)) {
                console.log("Successfully inserted person: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    updatePatient(patient) {
        return API_PATIENTS.updatePatient(patient, (result, status, error) => {
            console.log(result);
            if (result !== null && (status >= 200 && status < 300)) {
                console.log("Successfully updated person: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    handleSubmit() {
        debugger;
        let patient = {
            name: this.state.formControls.name.value.trim(),
            address: this.state.formControls.address.value.trim(),
            birthDate: this.state.formControls.birthDate.value.trim(),
            username: this.state.formControls.username.value.trim(),
            password: this.state.formControls.password.value.trim(),
            email: this.state.formControls.email.value.trim(),
            role: this.state.formControls.role.value.trim(),
            gender: this.state.formControls.gender.value.trim(),
            medicalRecord: this.state.formControls.medicalRecord.value.trim()
        };

        if (this.state.formControls.action.value === "DELETE") {
            this.deletePatient(parseInt(this.state.formControls.id.value.trim()))
        } else if (this.state.formControls.action.value === "UPDATE") {
            this.updatePatient({
                id: parseInt(this.state.formControls.id.value.trim()),
                name: patient.name,
                address: patient.address,
                password: patient.password,
                email: patient.email,
                medicalRecord: patient.medicalRecord
            })
        } else {
            debugger;
            this.registerPatient({
                user: {
                    id: null,
                    name: patient.name,
                    gender: patient.gender,
                    birthDate: patient.birthDate,
                    address: patient.address,
                    username: patient.username,
                    password: patient.password,
                    role: this.state.formControls.role.value,
                    email: patient.email
                },
                doctorId: parseInt(this.state.formControls.doctor.value),
                caregiverId: parseInt(this.state.formControls.caregiver.value),
                medicalRecord: patient.medicalRecord
            });
        }
    }

    render() {
        return (

            <form id="patientForm">

                <h1> Select Action: </h1>
                <select name="action" form="patientForm" onChange={this.handleChange}>
                    <option value="INSERT"> Insert </option>
                    <option value="UPDATE"> Update </option>
                    <option value="DELETE"> Delete </option>
                </select>

                <p></p>

                <h1>{this.state.formControls.action.value} patient</h1>

                {
                    (this.state.formControls.action.value === "DELETE" || this.state.formControls.action.value === "UPDATE") && <div>
                        <p> ID: </p>
                        <TextInput name="id"
                            placeholder={this.state.formControls.id.placeholder}
                            value={this.state.formControls.id.value}
                            onChange={this.handleChange}
                            touched={this.state.formControls.id.touched}
                            valid={this.state.formControls.id.valid}
                        />

                        {this.state.formControls.id.touched && !this.state.formControls.id.valid &&
                            <div className={"error-message row"}> * ID must have at least 1 characters </div>}
                    </div>
                }

                {
                    this.state.formControls.action.value !== "DELETE" &&
                    <div>
                        <p> Name: </p>

                        <TextInput name="name"
                            placeholder={this.state.formControls.name.placeholder}
                            value={this.state.formControls.name.value}
                            onChange={this.handleChange}
                            touched={this.state.formControls.name.touched}
                            valid={this.state.formControls.name.valid}
                        />
                        {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                            <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                    </div>
                }

                {
                    this.state.formControls.action.value !== "DELETE" &&
                    <div>
                        <p> Address: </p>
                        <TextInput name="address"
                            placeholder={this.state.formControls.address.placeholder}
                            value={this.state.formControls.address.value}
                            onChange={this.handleChange}
                            touched={this.state.formControls.address.touched}
                            valid={this.state.formControls.address.valid}
                        />
                        {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                            <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                    </div>
                }

                {
                    this.state.formControls.action.value === "INSERT" &&
                    <div>
                        <p> Birth Date: </p>
                        <TextInput name="birthDate"
                            placeholder={this.state.formControls.birthDate.placeholder}
                            value={this.state.formControls.birthDate.value}
                            onChange={this.handleChange}
                            touched={this.state.formControls.birthDate.touched}
                            valid={this.state.formControls.birthDate.valid}
                        />
                        {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                            <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                    </div>
                }

                {
                    this.state.formControls.action.value === "INSERT" &&
                    <div>
                        <p> Username: </p>
                        <TextInput name="username"
                            placeholder={this.state.formControls.username.placeholder}
                            value={this.state.formControls.username.value}
                            onChange={this.handleChange}
                            touched={this.state.formControls.username.touched}
                            valid={this.state.formControls.username.valid}
                        />
                    </div>
                }

                {
                    this.state.formControls.action.value !== "DELETE" &&
                    <div>
                        <p> Password: </p>
                        <PasswordInput name="password"
                            placeholder={this.state.formControls.password.placeholder}
                            value={this.state.formControls.password.value}
                            onChange={this.handleChange}
                            touched={this.state.formControls.password.touched}
                            valid={this.state.formControls.password.valid}
                        />
                        {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
                            <div className={"error-message"}> * Password must have at least 6 characters </div>}
                    </div>
                }

                {
                    this.state.formControls.action.value !== "DELETE" &&
                    <div>
                        <p> Email: </p>
                        <TextInput name="email"
                            placeholder={this.state.formControls.email.placeholder}
                            value={this.state.formControls.email.value}
                            onChange={this.handleChange}
                            touched={this.state.formControls.email.touched}
                            valid={this.state.formControls.email.valid}
                        />
                        {this.state.formControls.email.touched && !this.state.formControls.email.valid &&
                            <div className={"error-message"}> * Email must have a valid format</div>}
                    </div>
                }

                {
                    this.state.formControls.action.value === "INSERT" &&
                    <div>
                        <p> Gender: </p>
                        <select name="gender" form="personForm" onChange={this.handleChange}>
                            <option value="MALE"> Male </option>
                            <option value="FEMALE"> Female </option>
                        </select>
                    </div>
                }

                {
                    this.state.formControls.action.value === "INSERT" &&
                    <div>
                        <p> Doctor: </p>
                        <select name="doctor" form="personForm" onChange={this.handleChange}>
                            <option value="" selected disabled hidden> Choose a doctor </option>
                            {this.state.doctors.map((doctor, index) => (
                                <option value={doctor.id}> {doctor.name} </option>
                            ))}
                        </select>
                    </div>
                }

                {
                    this.state.formControls.action.value === "INSERT" &&
                    <div>
                        <p> Caregiver: </p>
                        <select name="caregiver" form="personForm" onChange={this.handleChange}>
                            <option value="" selected disabled hidden> Choose a caregiver </option>
                            {this.state.caregivers.map((caregiver, index) => (
                                <option value={caregiver.id}> {caregiver.name} </option>
                            ))}
                        </select>
                    </div>
                }

                {
                    this.state.formControls.action.value !== "DELETE" &&
                    <div>
                        <p> Medical Record: </p>
                        <TextInput name="medicalRecord"
                            placeholder={this.state.formControls.medicalRecord.placeholder}
                            value={this.state.formControls.medicalRecord.value}
                            onChange={this.handleChange}
                            touched={this.state.formControls.medicalRecord.touched}
                            valid={this.state.formControls.medicalRecord.valid}
                        />
                    </div>
                }

                <p></p>
                <Button variant="success"
                    type={"submit"}
                    disabled={!this.state.formIsValid}
                    onClick={this.handleSubmit}>
                    Submit
              </Button>

                {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />}

            </form>

        );
    }
}

export default PatientForm;

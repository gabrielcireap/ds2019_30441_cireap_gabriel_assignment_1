import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import PatientForm from "./patient-form";

import * as API_PATIENTS from "./api/patient-api"

const columns = [
	{
        Header:  'ID',
        accessor: 'id'
    },
	{
        Header:  'Name',
        accessor: 'name'
    },
    {
        Header:  'Address',
        accessor: 'address'
    },
    {
        Header: 'Email',
        accessor: 'email'
    },
	{
        Header:  'Birth Date',
        accessor: 'birthDate'
    },
	{
        Header:  'Gender',
        accessor: 'gender'
    },
	{
        Header:  'Doctor',
        accessor: 'doctor'
    },
	{
        Header:  'Caregiver',
        accessor: 'caregiver'
    },
	{
        Header:  'Medical Record',
        accessor: 'medicalRecord'
    }
];

const filters = [
	{
        accessor: 'name'
    },
    {
        accessor: 'address'
    },
    {
        accessor: 'email'
    },
	{
        accessor: 'birthDate'
    },
	{
        accessor: 'gender'
    },
	{
        accessor: 'doctor'
    },
	{
        accessor: 'caregiver'
    }
];

class Patients extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchPatients();
    }

    componentWillMount() {
        if (sessionStorage.getItem("Role") !== "DOCTOR") {
            window.location.assign("/")
        }
    }

    fetchPatients() {
        return API_PATIENTS.getPatients((result, status, err) => {
            console.log(result);
           if(result !== null && status === 200) {
               result.forEach( x => {
                   this.tableData.push({
						id: x.id,
						name: x.user.name,
						address: x.user.address,
						birthDate: x.user.birthDate,
						gender: x.user.gender,
						email: x.user.email,
						doctor: x.doctor.name,
						caregiver: x.caregiver.name,
						medicalRecord: x.medicalRecord
				   });
               });
               this.forceUpdate();
           } else {
               console.log("Am prins o eroare!!!");
               this.state.errorStatus = status;
               this.state.error = err;
               this.forceUpdate();
           }
        });
    }

    refresh() {
        this.forceUpdate()
    }

    render() {
         let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <PatientForm>	</PatientForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Patients;

import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import { Card, Col, Row } from 'reactstrap';
import Table from "../../commons/tables/table"

import * as API_PATIENTS from "./api/patient-api"

const columnsProfileInfo = [
    {
        Header: 'ID',
        accessor: 'id'
    },
    {
        Header: 'Name',
        accessor: 'name'
    },
    {
        Header: 'Address',
        accessor: 'address'
    },
    {
        Header: 'Email',
        accessor: 'email'
    },
    {
        Header: 'Birth Date',
        accessor: 'birthDate'
    },
    {
        Header: 'Gender',
        accessor: 'gender'
    },
    {
        Header: 'Doctor',
        accessor: 'doctor'
    },
    {
        Header: 'Caregiver',
        accessor: 'caregiver'
    },
    {
        Header: 'Medical Record',
        accessor: 'medicalRecord'
    }
];

const columnsMedicationPlan = [
    {
        Header: 'Medication ID',
        accessor: 'id'
    },
    {
        Header: 'Medication',
        accessor: 'name'
    },
    {
        Header: 'Side Effects',
        accessor: 'sideEffects'
    },
    {
        Header: 'Dosage',
        accessor: 'dosage'
    },
    {
        Header: 'Intake Moments',
        accessor: 'intakeMoments'
    },
    {
        Header: 'Start Date',
        accessor: 'startDate'
    },
    {
        Header: 'End Date',
        accessor: 'endDate'
    }
];

const filtersMedicationPlan = [
    {
        accessor: 'medication'
    },
    {
        accessor: 'sideEffects'
    },
    {
        accessor: 'dosage'
    },
    {
        accessor: 'intakeMoments'
    },
    {
        accessor: 'startDate'
    },
    {
        accessor: 'endDate'
    }
];

class PatientProfile extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableDataMedicationPlan = [];
        this.tableDataPatientInfo = [];
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    componentWillMount() {
        if (sessionStorage.getItem("Role") !== "PATIENT") {
            window.alert("Access denied!")
            window.location.assign("/")
        }

        this.fetchPatient(sessionStorage.getItem("Id"));
        this.fetchMedicationPlans(sessionStorage.getItem("Id"));
    }

    fetchPatient(patientId) {
        return API_PATIENTS.getPatientById(patientId, (result, status, err) => {
            console.log(result);
            if (result !== null && status === 200) {
                this.tableDataPatientInfo.push({
                    id: result.id,
                    name: result.user.name,
                    address: result.user.address,
                    birthDate: result.user.birthDate,
                    gender: result.user.gender,
                    email: result.user.email,
                    doctor: result.doctor.name,
                    caregiver: result.caregiver.name,
                    medicalRecord: result.medicalRecord
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    fetchMedicationPlans(userId) {
        debugger;
        return API_PATIENTS.getMedicationPlanByUserId(userId, (result, status, err) => {
            console.log(result);
            if (result !== null && status === 200) {
                result.forEach(medicationPlan => {
                    medicationPlan.medicationPerPlanDtoList.forEach(plan => {
                        this.tableDataMedicationPlan.push({
                            id: plan.medicationDto.id,
                            name: plan.medicationDto.name,
                            sideEffects: plan.medicationDto.sideEffects,
                            dosage: plan.dosage,
                            intakeMoments: plan.intakeMoments,
                            startDate: medicationPlan.startTime,
                            endDate: medicationPlan.endTime
                        });
                    })
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh() {
        this.forceUpdate()
    }

    render() {
        let pageSize = 5
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableDataPatientInfo}
                                columns={columnsProfileInfo}
                                search={[]}
                                pageSize={1}
                            />

                            <p></p>

                            <Table
                                data={this.tableDataMedicationPlan}
                                columns={columnsMedicationPlan}
                                search={filtersMedicationPlan}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>

                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />}

            </div>
        );
    };

}

export default PatientProfile;

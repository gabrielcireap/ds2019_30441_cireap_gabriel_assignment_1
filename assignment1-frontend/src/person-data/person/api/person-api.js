import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_persons: '/users',
    post_person: "/users",
	update_person: "/users",
	delete_person: "/users",
	login: "/login"
};

function getPersons(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_persons, {
        method: 'GET',
		headers: {
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
		}
    });
    console.log(request.url);
    debugger;
    RestApiClient.performRequest(request, callback);
}

function getPersonById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.get_persons + "/" + params.id, {
       method: 'GET',
	   headers: {
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
	   }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPerson(user, callback){
    let request = new Request(HOST.backend_api + endpoint.post_person , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updatePerson(user, callback){
    let request = new Request(HOST.backend_api + endpoint.update_person , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        },
        body: JSON.stringify(user)
    });
	debugger;
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletePerson(userId, callback){
    let request = new Request(HOST.backend_api + endpoint.delete_person + "/" + userId, {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(sessionStorage.getItem('Username') + ":" + sessionStorage.getItem('Password'))
        }
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function login(user, callback){
    let request = new Request(HOST.backend_api + endpoint.login , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
			'Authorization': "Basic " + btoa(user.username + ":" + user.password)
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getPersons,
    getPersonById,
    postPerson,
	updatePerson,
	deletePerson,
	login
};

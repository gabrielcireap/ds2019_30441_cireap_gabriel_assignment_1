import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import PersonForm from "./person-form";

import * as API_PATIENTS from "../../patient-data/patient/api/patient-api";

const columns = [
	{
        Header:  'ID',
        accessor: 'id'
    },
	{
        Header:  'Name',
        accessor: 'name'
    },
    {
        Header:  'Address',
        accessor: 'address'
    },
    {
        Header: 'Email',
        accessor: 'email'
    },
	{
        Header:  'Birth Date',
        accessor: 'birthDate'
    },
	{
        Header:  'Gender',
        accessor: 'gender'
    },
	{
        Header:  'Role',
        accessor: 'role'
    },
];

const filters = [
	{
        accessor: 'name'
    },
    {
        accessor: 'address'
    },
    {
        accessor: 'email'
    },
	{
        accessor: 'birthDate'
    },
	{
        accessor: 'gender'
    },
	{
        accessor: 'role'
    }
];

class Persons extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchDoctors();
		this.fetchCaregivers();
    }

    componentWillMount() {
        if (sessionStorage.getItem("Role") !== "DOCTOR") {
            window.alert("Access denied!")
            window.location.assign("/")
        }
    }

    fetchDoctors() {
        return API_PATIENTS.getDoctors((result, status, err) => {
            console.log(result);
           if(result !== null && status === 200) {
               result.forEach( x => {
                   this.tableData.push(x);
               });
               this.forceUpdate();
           } else {
               console.log("Am prins o eroare!!!");
               this.state.errorStatus = status;
               this.state.error = err;
               this.forceUpdate();
           }
        });
    }

	fetchCaregivers() {
		return API_PATIENTS.getCaregivers((result, status, err) => {
            console.log(result);
			if(result !== null && status === 200) {
               result.forEach( x => {
                   this.tableData.push(x);
               });
               this.forceUpdate();
			} else {
               console.log("Am prins o eroare!!!");
               this.state.errorStatus = status;
               this.state.error = err;
               this.forceUpdate();
			}
		});
	}

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <PersonForm registerPerson={this.refresh}>

                                </PersonForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Persons;
